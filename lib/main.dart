import 'package:drawer_app2/src/pages/first.dart';
import 'package:drawer_app2/src/pages/menu.dart';
import 'package:drawer_app2/src/pages/second.dart';
import 'package:flutter/material.dart';
// import 'package:drawer_app2/src/pages/home_page.dart';
import 'package:hidden_drawer_menu/hidden_drawer/hidden_drawer_menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: SimpleHiddenDrawer(
          isDraggable: true,
          curveAnimation: Curves.easeInOutBack,
          // slidePercent: 80,

          menu: MenuPage(),
          screenSelectedBuilder: (position, controller) {
            Widget screenCurrent;

            switch (position) {
              case 0:
                screenCurrent = FirstPage();
                break;
              case 1:
                screenCurrent = SecondPage();
                break;
            }
            return Scaffold(
              backgroundColor: Colors.green,
              appBar: AppBar(
                leading: IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: () {
                      controller.toggle();
                    }),
              ),
              body: screenCurrent,
            );
          }),
    );
  }
}
